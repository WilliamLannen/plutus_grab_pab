# Plutus Grab PAB - Postman Collection

This is a Postman collection for testing a simple PAB example using the Give/Grab contract from [Lecture 2 of the Plutus Pioneer Program](https://github.com/input-output-hk/plutus-pioneer-program/tree/main/code/week02).

Note this collection assumes that the Funds endpoint from [Lecture 6](https://github.com/input-output-hk/plutus-pioneer-program/tree/main/code/week06) is also included in order to facilate testing (despite making the solution not as 'minimal' as possible). If your solution doesn't have the Funds endpoint, simply ignore those calls and observe the Gift/Grab happening via console logs.

## Instructions
1. Import the collection and environment.
2. Ensure the 'Local PAB' environment is active and set the `contract_instance` variable to the contract instance ID of one of the wallets initialised in your code.
3. Call the `GET Contract Status` endpoint and ensure a payload is returned.
4. Call the `POST Funds` endpoint. Note that an empty array will be returned. This is expected - the call updates the contract state with the funds data, but doesn't return it.
5. Call the `GET Contract Status` endpoint again and observe that the funds data is now present.
6. Call the `POST Give` endpoint with a suitable amount of lovelace (note that as per the collection the payload is simply the number of lovelace).
7. Repeat 4 & 5 to observe the updated Funds data.
8. Change the `contract_instance` variable in the environment to the contract instance ID of a different wallet.
9. Repeat 4 & 5 to observe the Funds data for this wallet.
10. Call `POST Grab` endpoint (note that as per the collection the payload is simply the integer to supply as the redeemer, i.e. 42 for a successful validation).
11. Repeat 4 & 5 to observe the Funds data for the wallet - updated to reflect the successful Grab.
